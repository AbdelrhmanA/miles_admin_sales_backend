const passport = require('passport')
const LocalStrategy = require('passport-local').Strategy
const { userModelSelectUserData } = require('../models')
const { compareSync } = require('bcrypt')

passport.use('local', new LocalStrategy({ usernameField: 'phone', passwordField: 'password' }
    , function (phone, password, done) {
        userModelSelectUserData({ one: true, finder: { 'phone.number': phone } }).then(response =>
            response ? compareSync(password, response.password) ? done(null, response) : done(null, false)
                : done(null, false, response))
    }))

passport.serializeUser((user, done) => done(null, user.phone.number))

passport.deserializeUser((phone, done) =>
    userModelSelectUserData({
        aggregation: [
            { $match: { 'phone.number': phone } },
            {
                $lookup: {
                    from: "roles",
                    localField: "role",
                    foreignField: "name",
                    as: "field"
                }
            },
            { $addFields: { permissions: "$field.permissions" } },
            { $unwind: "$permissions" },
            { $project: { field: 0 } }
        ]
    }).then(user => user[0] ? done(null, user[0]) : done(null, null)))