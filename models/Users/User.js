const { Schema, model } = require('mongoose')

const UserSchema = new Schema({
    firstName: { type: String, required: true },
    lastName: { type: String, required: true },
    userName: { type: String, unique: true },
    gender: { type: String, enum: ['Male', "Female"] },
    email: { type: String, required: true, unique: true },
    mobileNumber: { type: { countryCode: { type: String, required: true }, number: { type: String, required: true } }, required: true },
    address: { type: String },
    password: { type: String, required: true },
    isBlocked: { type: Boolean, default: false }
})

UserSchema.pre('save', function createUsernameForUser() {
    this.userName = `${this.firstName[0]}${this.lastName[0]}${Math.floor(Math.random() * (100000 - 999999)) + 100000}`
})

const UserModel = model('User', UserSchema)


module.exports = UserModel