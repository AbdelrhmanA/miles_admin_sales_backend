const { Schema, Model } = require('mongoose')

const RequestSchema = new Schema({
    productName = { type: String, required: true },
    category: { type: Schema.Types.ObjectId, ref: 'Category', required: true },
    createdBy: { type: { user: { type: Schema.Types.ObjectId, ref: 'User' }, isAdmin: { type: Boolean, default: false } }, required: true },
    type: { type: String, enum: ['BUY', "SELL"] },
    price: { type: Number, min: 1, required: true },
    quantity: { type: Number, min: 1, required: true },
    userInfo: {
        type: {
            firstName: { type: String, required: true },
            lastName: { type: String, required: true },
            mobileNumber: { type: { countryCode: { type: String, required: true }, number: { type: String, required: true } } },
            email: { type: String, required: true },
        }
    }
})

RequestSchema.pre('save', function checkIfRequestCreatedByAdmin(next) {
    if (this.createdBy.isAdmin && !this.userInfo) next(Error('User information must be provided when admin create a request on behalf of user!'))
})

const RequestModel = Model('Request', RequestSchema)


module.exports = RequestModel