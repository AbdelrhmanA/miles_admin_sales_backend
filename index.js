
const express = require('express')
const { ApolloServer } = require('apollo-server-express')
const session = require('express-session')
const MongoStore = require('connect-mongo')(session)
const passport = require('passport')
const mongoose = require('mongoose')
const compression = require('compression')
const helmet = require('helmet')
const cors = require('cors')
const graphql = require('./graphql')
require('dotenv').config()
// require('./configs/passport-setup')

mongoose.connect(process.env.MONGO_URL, { useNewUrlParser: true, autoReconnect: true, useCreateIndex: true, useFindAndModify: false, useUnifiedTopology: true })
    .then(_ => console.log("MongoDB connected!"))
const app = express()
const server = new ApolloServer({ ...graphql, context: ({ req, res }) => ({ req, res }) })
// app.use(favicon('favicon.ico'))
app.use(cors({ origin: RegExp('/*/'), credentials: true }))
app.use(compression())
app.use(helmet())
app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use(session({
    secret: process.env.SESSION_SECRET,
    saveUninitialized: false,
    resave: false,
    rolling: true,
    cookie: { maxAge: 86400000, httpOnly: false }
    , store: new MongoStore({ url: process.env.MONGO_URL, autoRemove: 'native', autoReconnect: true })
}))


server.applyMiddleware({ app, cors: { origin: RegExp('/*/'), credentials: true } })


app.listen({ port: process.env.PORT }, () => console.log(`http://localhost:6060${server.graphqlPath}`))




/*
TODO
app.use(express.static(join(__dirname, admin)))


// app.use(passport.initialize())
// app.use(passport.session())


// app.use('/images', require('./service/image-server'))
// app.use('*', require('./service/admin-panel'))


// const favicon = require('serve-favicon')
// const { mongodb: { URI, OPTIONS }, PORT: port, CORS, SESSION, SESSION_STORE } = require('./configs/settings')
// const { initializeSystem, admin } = require('./utils')
// const { join } = require('path')

*/