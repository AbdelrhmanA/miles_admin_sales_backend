module.exports = `
getBranches(company: String!): [BRANCH]
getBranchDetails(branch: String): BRANCH
getBranchesTree(warehouse: String): [BRANCH_TREE]
getBranchProducts(branch: String): [BRANCH_PRODUCT]
`