module.exports = `
getSubCategories(parent: String): [CATEGORY]
listCategoriesNames: [CATEGORY_TREE]
getCategoryDetails(_id: String!): CATEGORY
`