module.exports = `
listWarehouses: [WAREHOUSE]
warehouseDetails(_id: String!): WAREHOUSE
getCoverageInitials: WAREHOUSE_COVERAGE
getAddProductToWarehouseInitials: WAREHOUSE_PRODUCT_ADD
`