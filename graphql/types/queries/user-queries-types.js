const { permissions: { userManagement } } = require('../../../utils')
module.exports = `
login(phone: String!, password: String!): USER
logout: Boolean
getAllUsers: [USER] @IsAuthorized(requires: ["${userManagement}"])
getUserDetails(phone: String!): USER @IsAuthorized(requires: ["${userManagement}"])
getUserAddresses(phone: String!): [ADDRESS] @IsAuthorized(requires: ["${userManagement}"])
`