module.exports = `
getCityAreas(city: String): [AREA]
getSubAreas(area: String!): [AREA]
getAreaTree: [AREA_TREE]
`