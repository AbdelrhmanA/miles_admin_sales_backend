const { permissions: { roleManagement } } = require('../../../utils')
module.exports = `
getRoles:[ROLE] @IsAuthorized(requires:["${roleManagement}"])
getPermissions:[String] @IsAuthorized(requires:["${roleManagement}"])
`