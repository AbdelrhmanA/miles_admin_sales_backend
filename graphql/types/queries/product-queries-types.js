const { permissions: { userManagement } } = require('../../../utils')

module.exports = `
getAllProducts: [PRODUCT] @IsAuthorized(requires: ["${userManagement}"])
getProductDetails(id: String!): PRODUCT @IsAuthorized(requires: ["${userManagement}"])
productsNames: [PRODUCT_LIST]
`