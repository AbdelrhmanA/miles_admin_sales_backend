module.exports = `
createTaxes(taxes: [INPUT_TAX]!): Boolean
updateTax(tax: INPUT_TAX_EDIT!): Boolean
deleteTax(id: String): Boolean
`