module.exports = `
addBranch(branch: INPUT_BRANCH!): BRANCH
editBranch(branch: INPUT_BRANCH!): Boolean
addUserToBranch(users: [String], branchId: String, companyId: String): Boolean
editBranchUsers(users:[String], branchId: String): Boolean
editBranchProductPrice(branch: String, product: String, price: String): Boolean
deleteBranchProduct(branch: String, product: String): Boolean
`