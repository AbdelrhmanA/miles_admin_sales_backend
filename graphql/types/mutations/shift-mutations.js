module.exports = `
addShift(shift: INPUT_SHIFT): SHIFT
updateShift(id: String, shift: INPUT_SHIFT): Boolean
toggleShiftState(id: String, value: Boolean): Boolean
addTimeSlot(id: String, timeSlot: INPUT_TIMESLOTS_TYPE): Boolean
deleteTimeSlot(shiftId: String, slotId: String ): Boolean
updateTimeSlot(shiftId: String, slotId: String, timeSlot: INPUT_TIMESLOTS_TYPE): Boolean
`