const { userManagement } = require('../../../utils').permissions

module.exports = `
addProduct(product: INPUT_PRODUCT!): PRODUCT @IsAuthorized(requires: ["${userManagement}"])
updateProduct(product: INPUT_PRODUCT!): Boolean @IsAuthorized(requires: ["${userManagement}"])
`