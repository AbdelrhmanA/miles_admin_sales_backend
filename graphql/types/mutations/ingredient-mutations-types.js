module.exports = `
createIngredient(ingredient: INPUT_INGREDIENT!): INGREDIENT
updateIngredient(ingredient: INPUT_INGREDIENT!, id: String!): Boolean
deleteIngrdient(id: String!): Boolean
`