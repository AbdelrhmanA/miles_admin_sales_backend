// const { userManagement } = require('../../../utils').permissions

module.exports = `
register(user: INPUT_USER!): Boolean 
`

/*
register(user: INPUT_USER!): USER
updateUser(user: INPUT_USER!, id: String!): Boolean @IsAuthorized(requires: ["${userManagement}"])
blockUnBLock(phone: String!, value: Boolean!): Boolean @IsAuthorized(requires: ["${userManagement}"])
addUserAddress(phone: String!, address: INPUT_ADDRESS!): Boolean @IsAuthorized(requires: ["${userManagement}"])
deleteUserAddress(phone: String!, id: String!): Boolean @IsAuthorized(requires: ["${userManagement}"])
updateUserAddress(phone: String!, id: String!, address: INPUT_ADDRESS!): Boolean @IsAuthorized(requires: ["${userManagement}"])
makeAddressDefault(phone: String!, id: String!): Boolean @IsAuthorized(requires: ["${userManagement}"])
*/