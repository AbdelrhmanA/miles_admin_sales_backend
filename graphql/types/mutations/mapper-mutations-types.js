module.exports = `
editCoverage(coverage: INPUT_COVERAGE!): Boolean
addProductToWarehouse(warehouseProduct: INPUT_WAREHOUSE_PRODUCT!): Boolean
deleteWarehouseAreaProduct(_id: String): Boolean
updateWarehouseBranchProduct(warehouse: String, shift: String, branch: String, product: INPUT_PRODUCT_WAREHOUSE): Boolean
`
