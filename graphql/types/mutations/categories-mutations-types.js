module.exports = `
addCategory(category: INPUT_CATEGORY!): String
editCategory(category: INPUT_CATEGORY!): Boolean
deleteCategory(id: String!): Boolean
`