module.exports = `
type CUSTOM_DISPLAY {
    country: String
    name: LOCALIZED_LANG_TYPE
}

type CATEGORY {
    _id: String
    path: [String]
    name: LOCALIZED_LANG_TYPE
    description: LOCALIZED_LANG_TYPE
    webImage: LOCALIZED_LANG_TYPE
    mobileImage: LOCALIZED_LANG_TYPE
    customDisplay: [CUSTOM_DISPLAY]
    subsCount: Int
    childrenProducts: Int
    parents: [String]
}

type CATEGORY_TREE {
    value: String
    label: String
    children: [CATEGORY_TREE]
}
`