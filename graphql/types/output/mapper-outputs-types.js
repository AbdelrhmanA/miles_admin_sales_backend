module.exports = `

type COVERAGE_BRANCH {
    branch: String
    shifts: [String]
    status: Boolean
}

type COVERAGE_AREA {
    area: [String]
    shifts: [String]
    status: Boolean
}

type COVERAGE {
    branches: [COVERAGE_BRANCH]
    areas: [COVERAGE_AREA]
}

type STOCK_PRODUCTS_TYPE {
    stock: String
    stockBased: Boolean
    product: String
    visiablity: Boolean
}
type AREA_PRODUCTS_TYPE {
    details: STOCK_PRODUCTS_TYPE
    name: String
    shift: String
    area: String
    warehouse: String
}

type ID_TYPE {
    warehouse: String
    shift: String
    area: String
}

type AREA_PRODUCTS { 
    products: [AREA_PRODUCTS_TYPE]
    _id: ID_TYPE
}
`