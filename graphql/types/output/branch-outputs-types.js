module.exports = `
type LOCATION_TYPE {
    lat: Float
    lng: Float 
}
type BRANCH {
    _id: String
    name: LOCALIZED_LANG_TYPE!
    area: [String!]
    address: String
    deliveryFees: Float
    label: String
    company: COMPANY_TYPE
    status: Boolean
    location: LOCATION_TYPE
    users: [BRANCH_USER]
    warehouses: [String]
    attachedShifts: [String]
    shifts(warehouse: String!): [SHIFT]
}
type BRANCH_TREE {
    value: String
    label: String
}
type BRANCH_USER {
    _id: String
    name: LOCALIZED_TYPE
}
type BRANCH_PRODUCT {
    price: String
    visiablity: Boolean
    stock: String
    branchId: String
    productId: String
    stockBased: Boolean
    name: LOCALIZED_LANG_TYPE
}
`