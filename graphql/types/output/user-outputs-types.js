module.exports = `
type LOCALIZED_TYPE {
    first: String
    last: String
}

type LOCALIZED_LANG_TYPE {
    en: String
    ar: String
}

type PHONE {
    countryCode: String
    number: String
}

type BALANCE {
    customer: Float
    driver: Float
}

type SOCIAL {
    channel: String
    token: String
}

type COMPANY {
    company: String
    branch: String
    role: String
}

type LOCATION {
    long: Float
    lat: Float
    address: String
    byWho: String
    }

type ADDRESS {
    _id: String
    name: LOCALIZED_TYPE
    isDefault: Boolean
    country: String
    city: String
    area: [String]
    address: String
    deliveryInstructions: String
    phone: PHONE
    label: String
    cxNote: String
    location: LOCATION
}

type USER {
    _id: String
    name: LOCALIZED_TYPE
    email: String
    isVerified: Boolean
    isRegistered: Boolean
    isBlocked: Boolean
    phone: PHONE
    role: String
    balance: BALANCE
    lang: String
    channel: String
    social: SOCIAL
    comapnies: [COMPANY]
    adresses: [ADDRESS]
    permissions: [String]
}
`