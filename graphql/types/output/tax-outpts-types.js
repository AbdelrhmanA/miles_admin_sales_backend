module.exports = `
type PURE_TAX {
    _id: String
    name: LOCALIZED_LANG_TYPE
    value: Float
    included: Boolean
    isDeleted: Boolean
    deletable: Boolean
}
type TAX_LIST {
    _id: String
    count: Int
    taxes: [PURE_TAX]
}
type TAX_TREE {
    value: String
    label: String
    children: [TAX_TREE]
}
`