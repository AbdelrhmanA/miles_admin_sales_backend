module.exports = `
type WAREHOUSE {
    _id: String
    name: String
    status: Boolean
    sum: Int
    areas: [AREA]
    branches: [BRANCH]
}

type WAREHOUSE_COVERAGE {
    areasTree: [AREA_TREE]
    shiftsTree: [SHIFT_TREE]
    branchesTree: [BRANCH_TREE]
    coverage(warehouse: String!): COVERAGE
}

type WAREHOUSE_PRODUCT_ADD {
    customProductList: [CUSTOM_LIST]
    shiftAreaBranch(warehouse: String!): [SHIFT_TREE_BRANCH]
    branchTree(warehouse: String!): [BRANCH_TREE]
    areaLeaves(warehouse: String!): [AREA_NODE]
}
`