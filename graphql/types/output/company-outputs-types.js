module.exports = `
type COMPANY_TYPE {
    _id: String
    name: LOCALIZED_LANG_TYPE
    branchesCount: Int
    status: Boolean
    website: String
    logo: String
    country: String
}
`