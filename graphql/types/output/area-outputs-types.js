module.exports = `
type AREA {
    _id: String
    isEnabled: Boolean
    isInternal: Boolean
    name: LOCALIZED_LANG_TYPE
    subsCount: Int
    parent: String
    shifts(warehouse: String!): [SHIFT]
}
type AREA_TREE {
    value: String
    label: String
    children: [AREA_NODE]
}
type AREA_NODE {
    value: String
    label: String
    children: [AREA_NODE]
}
`