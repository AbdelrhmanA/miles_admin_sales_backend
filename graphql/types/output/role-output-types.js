module.exports = `
type ROLE {
    name: String
    permissions: [String]
    deletable: Boolean
}
`