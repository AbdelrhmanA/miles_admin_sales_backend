module.exports = `
type INGREDIENT {
    deletable: Boolean
    name: String
    _id: String
} 
`