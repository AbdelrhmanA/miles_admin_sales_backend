module.exports = `
type LOCALIZED_COUNTRY_NAME {
    en: String
    ar: String
    short: String
}
type CITY {
    _id: String
    name: LOCALIZED_LANG_TYPE
    countAreas: Int
}

type COUNTRY_TYPE {
    _id: String
    name: LOCALIZED_COUNTRY_NAME
    code: String
    cities: [CITY]
    countCities: Int
    countAreas: Int
}

type COUNTRY_SHORT_NAME {
    label: String
    value: String
}
`