module.exports = `
type GROUP {
    product: String
    count: Int
}
type LOCALIZED_PRICING {
    country:String
    price: Float
}
type PRICE {
    countries: [LOCALIZED_PRICING]
    branches: [LOCALIZED_PRICING]
}
type PRODUCT_INGREDIENT {
    name: String
    number: Float
    unit: String
}
type PRODUCT_ATTRIBUTE {
    name: LOCALIZED_LANG_TYPE
    options: [String]
    price: Float
}
type CREATEION_TYPE{
    date: String
    byWho: String
}
type PRODUCT_TAXES {
    countryShort: String
    tax: String
    includePrice: Boolean
}
type PRODUCT_LIST {
    label: String
    value: String
}
type PRODUCT {
    _id: String
    entity: String
    sku: String!
    brand: [String]
    tags: [String]
    groupProduct: Boolean
    name: LOCALIZED_LANG_TYPE
    description: LOCALIZED_LANG_TYPE
    featuredImage: LOCALIZED_LANG_TYPE
    gallery: [LOCALIZED_LANG_TYPE]
    price: PRICE
    group: [GROUP]
    relatedProducts: [String]
    category: [String]
    internalCategory: String
    productIngredients: [PRODUCT_INGREDIENT]
    productAttributes: [PRODUCT_ATTRIBUTE]
    storageInstructions: String
    status: Boolean
    private: Boolean
    creation: CREATEION_TYPE
    stock: Float
    taxes: [PRODUCT_TAXES]
}
type CUSTOM_LIST {
    _id: String
    name: String
    category: String
    status: Boolean
    price: Float
    image: String
}
`