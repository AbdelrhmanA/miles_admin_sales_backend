module.exports = `
type PRODUCT_SHIFT {
    product: String
    _id: String
    name: String
    stock: String
    stockBased: Boolean
    visiablity: Boolean
}

type SHIFT {
    _id: String
    name: String
    isDisabled: Boolean
    timeSlots: [TIMESLOTS_TYPE]
    areaProducts(warehouse: String!): [PRODUCT_SHIFT]
    branchProducts(warehouse: String!): [PRODUCT_SHIFT]
}
    
type TIMESLOTS_TYPE {
    _id: String
    from: String
    to: String
    deliveryFee: String
    cutOffType: CUTOFF_TYPE
}
    
type CUTOFF_TYPE {
    type: String
    value: String
}

type SHIFT_TREE {
    value: String
    label: String
}
type SHIFT_TREE_BRANCH {
    value: String
    label: String
    coverage: [String]
}
`