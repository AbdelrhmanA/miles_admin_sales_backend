module.exports = `
input INPUT_AREA_MAPPER {
    area: [String]
    shifts: [String!]
    status: Boolean
}
input INPUT_BRANCH_MAPPER {
    branch: String
    shifts: [String!]
    status: Boolean
}
input INPUT_COVERAGE {
    id: String!
    areas: [INPUT_AREA_MAPPER!]
    branches: [INPUT_BRANCH_MAPPER!]
}
input INPUT_WAREHOUSE_PRODUCTS {
    product: String!
    stockBased: Boolean
    stock: Int
}
input INPUT_WAREHOUSE_BRANCH {
    first: [String]!
    second: [String]!
}
input INPUT_WAREHOUSE_AREA {
    first: [String]!
    second: [String]!
}
input INPUT_WAREHOUSE_PRODUCT {
    warehouse: String!
    areas: INPUT_WAREHOUSE_AREA
    branches: INPUT_WAREHOUSE_BRANCH
    products: [INPUT_WAREHOUSE_PRODUCTS!]
}
input INPUT_PRODUCT_WAREHOUSE {
    stock: String
    stockBased: Boolean
    visiablity: Boolean
    product: String
    _id: String
}
`