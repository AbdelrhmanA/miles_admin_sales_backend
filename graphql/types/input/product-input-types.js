module.exports = `
input INPUT_GROUP {
    product: String
    count: Int
}
input INPUT_LOCALIZED_PRICING {
    country: String
    price: Float
}
input INPUT_PRICE {
    countries: [INPUT_LOCALIZED_PRICING]
    branches: [INPUT_LOCALIZED_PRICING]
}
input INPUT_PRODUCT_INGREDIENT {
    name: String
    number: Float
    unit: String
}
input INPUT_PRODUCT_ATTRIBUTE {
    name: INPUT_LOCALIZED_LANG_TYPE
    options: [String]
    price: Float
}
input INPUT_PRODUCT_TAXES {
    countryShort: String
    includePrice: Boolean
    tax: String
}
input INPUT_PRODUCT {
    id: String
    entity: String!
    sku: String!
    brand: [String]
    tags: [String]
    groupProduct: Boolean
    internalCategory: String
    name: INPUT_LOCALIZED_LANG_TYPE!
    description: INPUT_LOCALIZED_LANG_TYPE
    featuredImage: INPUT_LOCALIZED_LANG_TYPE
    gallery: [INPUT_LOCALIZED_LANG_TYPE]
    price: INPUT_PRICE
    group: [INPUT_GROUP]
    relatedProducts: [String]
    category: [String]
    productIngredients: [INPUT_PRODUCT_INGREDIENT]
    productAttributes: [INPUT_PRODUCT_ATTRIBUTE]
    storageInstructions: String
    status: Boolean
    private: Boolean
    taxes: [INPUT_PRODUCT_TAXES]
}
`