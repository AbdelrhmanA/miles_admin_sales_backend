module.exports = `
input INPUT_SHIFT { 
    name: String!
    timeSlots: [INPUT_TIMESLOTS_TYPE]
}

input INPUT_TIMESLOTS_TYPE {
    from: String
    to: String
    deliveryFee: String
    cutOffType: INPUT_CUTOFF_TYPE
}

input INPUT_CUTOFF_TYPE {
    type: String
    value: String
}

`