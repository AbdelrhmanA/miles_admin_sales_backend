module.exports = `
input INPUT_LOCALIZED_TYPE {
    first: String
    last: String
}

input INPUT_PHONE {
    countryCode: String
    number: String
}

input INPUT_LOCATION {
    long: Float
    lat: Float
    address: String
    byWho: String
    }

input INPUT_ADDRESS {
    name: INPUT_LOCALIZED_TYPE
    isDefault: Boolean
    country: String
    city: String
    area: [String]
    address: String
    deliveryInstructions: String
    phone: INPUT_PHONE
    label: String
    cxNote: String
    location: INPUT_LOCATION
}

input INPUT_USER {
    firstName: String!
    lastName: String!
    mobileNumber: INPUT_PHONE!
    address: String!
    password: String!
    email: String!
    gender: String
}
`