module.exports = `
input INPUT_LOCALIZED_COUNTRY_NAME {
    en: String!
    ar: String!
    short: String
}
input INPUT_CITY {
    name: INPUT_LOCALIZED_LANG_TYPE!
}
input INPUT_COUNTRY_TYPE {
    name: INPUT_LOCALIZED_COUNTRY_NAME!
    code: String!
    cities: [INPUT_CITY]!
}
`