module.exports = `
input INPUT_TAX {
    name: INPUT_LOCALIZED_LANG_TYPE!
    countryCode: String!
    value: Float!
    included: Boolean
}
input INPUT_TAX_EDIT {
    id: String!
    name: INPUT_LOCALIZED_LANG_TYPE!
    value: Float!
    included: Boolean
}
`