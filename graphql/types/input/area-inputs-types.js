module.exports = `
input INPUT_AREA {
    path: [String]!
    name: INPUT_LOCALIZED_LANG_TYPE!
    isEnabled: Boolean
    isInternal: Boolean
}
`