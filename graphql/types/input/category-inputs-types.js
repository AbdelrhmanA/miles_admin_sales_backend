module.exports = `
input INPUT_CUSTOM_DISPLAY {
    country: String
    name: INPUT_LOCALIZED_LANG_TYPE
}
input INPUT_CATEGORY {
    id: String
    path: [String]
    name: INPUT_LOCALIZED_LANG_TYPE!
    description: INPUT_LOCALIZED_LANG_TYPE
    webImage: INPUT_LOCALIZED_LANG_TYPE
    mobileImage: INPUT_LOCALIZED_LANG_TYPE
    customDisplay: [INPUT_CUSTOM_DISPLAY]
}
`