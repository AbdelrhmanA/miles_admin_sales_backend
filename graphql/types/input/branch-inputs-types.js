module.exports = `
input INPUT_LOCATION_TYPE {
    lat: Float
    lng: Float 
}

input INPUT_BRANCH_USER {
    _id: String
    name: INPUT_LOCALIZED_TYPE
}


input INPUT_BRANCH {
    _id: String
    name: INPUT_LOCALIZED_LANG_TYPE!
    company: String!
    area: [String!]
    address: String
    deliveryFees: Float
    label: String
    status: Boolean
    location: INPUT_LOCATION_TYPE
    warehouses: [String]
    users: [INPUT_BRANCH_USER]
    attachedShifts: [String]
}
`