module.exports = `
input INPUT_COMPANY_TYPE {
    id: String
    name: INPUT_LOCALIZED_LANG_TYPE!
    country: String!
    logo: String
    status: Boolean
    website: String
}
`