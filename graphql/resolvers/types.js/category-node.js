const { categoriesModelFind } = require('../../../models')
module.exports = {
    children: ({ _id: parent }) =>
        categoriesModelFind({ aggregation: [{ $match: { parent } }, { $project: { value: "$_id", label: "$name.en", } }] })
}