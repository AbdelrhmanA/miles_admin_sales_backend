const { productModelSelectProducts, mappersModelFind } = require('../../../models')
const { Types: { ObjectId } } = require('mongoose')

module.exports = {
    customProductList: () => productModelSelectProducts({
        aggregation: [
            {
                $lookup: {
                    from: 'categories',
                    let: { c: '$category' },
                    pipeline: [
                        { $match: { $expr: { $in: ["$_id", '$$c'] } } },
                        { $project: { _id: 0, name: "$name.en" } }
                    ],
                    as: 'category'
                }
            },
            { $unwind: { path: '$category', preserveNullAndEmptyArrays: true } },
            {
                $group: {
                    _id: "$_id",
                    category: { $first: "$category.name" },
                    name: { $first: "$name.en" },
                    status: { $first: "$private" },
                    price: { $first: { $arrayElemAt: ["$price.countries.price", 0] } },
                    image: { $first: "$featuredImage.en" }
                }
            }
        ]
    }),
    shiftAreaBranch: (_, { warehouse }) => mappersModelFind({
        aggregation: [
            {
                $match: {
                    warehouse: new ObjectId(warehouse)
                }
            },
            { $addFields: { isArea: { $cond: [{ $not: ["$branch"] }, true, false] } } },
            {
                $group: {
                    _id: "$shift",
                    coverage: {
                        $push: {
                            $cond: [
                                { $eq: ["$isArea", true] },
                                { $arrayElemAt: ["$area", -1] },
                                "$branch"
                            ]
                        }
                    }
                }
            },
            {
                $lookup: {
                    from: 'shifts',
                    let: { id: "$_id" },
                    pipeline: [
                        {
                            $match: {
                                $expr: { $eq: ["$$id", "$_id"] }
                            }
                        },
                        { $project: { name: 1, _id: 0 } }
                    ],
                    as: 'name'
                }
            },
            { $unwind: { path: "$name" } },
            {
                $project: {
                    _id: 0,
                    value: "$_id",
                    label: "$name.name",
                    coverage: "$coverage",
                }
            }
        ]
    }),
    branchTree: (_, { warehouse }) => mappersModelFind({
        aggregation: [
            { $match: { warehouse: ObjectId(warehouse) } },
            {
                $lookup: {
                    from: 'branches',
                    localField: 'branch',
                    foreignField: '_id',
                    as: 'branch'
                }
            },
            { $unwind: { path: "$branch" } },
            {
                $group: {
                    _id: "$branch._id",
                    label: { $first: "$branch.name.en" },
                    value: { $first: "$branch._id" }
                }
            }
        ]
    }),
    areaLeaves: (_, { warehouse }) => mappersModelFind({
        aggregation: [
            { $match: { warehouse: ObjectId(warehouse) } },
            { $project: { id: 1, area: { $arrayElemAt: ["$area", -1] } } },
            {
                $lookup: {
                    from: 'areas',
                    localField: 'area',
                    foreignField: '_id',
                    as: 'area'
                }
            },
            { $unwind: { path: "$area" } },
            {
                $group: {
                    _id: "$area._id",
                    label: { $first: "$area.name.en" },
                    value: { $first: "$area._id" }
                }
            }
        ]
    })
}