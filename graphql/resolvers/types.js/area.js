const { mappersModelFind } = require('../../../models')
const { Types: { ObjectId } } = require('mongoose')

module.exports = {
    shifts: ({ _id: area }, { warehouse }) => mappersModelFind({
        aggregation: [
            { $match: { area, warehouse: new ObjectId(warehouse) } },
            { $group: { _id: "$warehouse", shifts: { $push: "$shift" }, area: { $first: '$area' } } },
            { $lookup: { from: 'shifts', localField: 'shifts', foreignField: '_id', as: 'shifts' } },
            { $unwind: { path: "$shifts" } },
            { $addFields: { "shifts.area": { $arrayElemAt: ["$area", -1] } } },
            { $replaceRoot: { newRoot: "$shifts" } }
        ]
    })
}