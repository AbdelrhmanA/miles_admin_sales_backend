const { areasModelFind } = require('../../../models')
module.exports = {
    children: ({ value, city }) => city ?
        areasModelFind({ aggregation: [{ $match: { "path.1": value, parent: { $exists: false } } }, { $project: { _id: 0, value: "$_id", label: "$name.en", } }] }) :
        areasModelFind({ aggregation: [{ $match: { parent: value } }, { $project: { _id: 0, value: "$_id", label: "$name.en", } }] })
}