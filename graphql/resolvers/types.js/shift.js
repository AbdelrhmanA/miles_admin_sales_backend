const { mappersModelFind } = require('../../../models')
const { Types: { ObjectId } } = require('mongoose')
module.exports = {
    areaProducts: ({ area, _id: shift }, { warehouse }) => mappersModelFind({
        aggregation: [
            { $match: { warehouse: new ObjectId(warehouse), area, shift } },
            { $unwind: { path: "$products" } },
            { $replaceRoot: { newRoot: "$products" } },
            { $lookup: { from: 'products', localField: 'product', foreignField: '_id', as: 'name' } },
            { $addFields: { name: "$name.name.en" } },
            { $unwind: { path: "$name" } }
        ]
    }),
    branchProducts: ({ branch, _id: shift }, { warehouse }) =>
        mappersModelFind({
            aggregation: [
                { $match: { warehouse: new ObjectId(warehouse), branch, shift } },
                { $unwind: { path: "$products" } },
                { $replaceRoot: { newRoot: "$products" } },
                { $lookup: { from: 'products', localField: 'product', foreignField: '_id', as: 'name' } },
                { $addFields: { name: "$name.name.en" } },
                { $unwind: { path: "$name" } }
            ]
        })
}