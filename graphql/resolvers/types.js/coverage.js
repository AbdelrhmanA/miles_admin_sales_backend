const { countiresModelFind, shiftModelfindShifts, branchesModelFind, mappersModelFind } = require('../../../models')
const { Types: { ObjectId } } = require('mongoose')

module.exports = {
    areasTree: () => countiresModelFind({
        aggregation: [
            {
                $project: {
                    _id: 0,
                    value: "$_id",
                    label: "$name.en",
                    children: {
                        $map: {
                            input: "$cities",
                            as: "sub",
                            in: {
                                value: "$$sub._id",
                                label: "$$sub.name.en",
                                city: true
                            }
                        }
                    }
                }
            }
        ]
    }),
    shiftsTree: () => shiftModelfindShifts({
        aggregation: [
            {
                $project: {
                    _id: 0,
                    value: "$_id",
                    label: "$name",
                }
            }
        ]
    }),
    branchesTree: () => branchesModelFind({ aggregation: [{ $project: { _id: 0, value: "$_id", label: "$name.en", } }] }),
    coverage: (_, { warehouse }) => mappersModelFind({
        aggregation: [
            { $match: { warehouse: ObjectId(warehouse) } },
            { $addFields: { isArea: { $cond: [{ $not: ["$branch"] }, true, false] } } },
            {
                $group: {
                    _id: { area: "$area", branch: "$branch", warehouse: "$warehouse" },
                    shift: { $push: "$shift" },
                    area: { $first: "$area" },
                    branch: { $first: "$branch" },
                    warehouse: { $first: "$warehouse" },
                    isArea: { $first: "$isArea" },
                    status: { $first: "$status" },
                    products: { $first: "$products" }

                }
            },
            {
                $group: {
                    _id: "$warehouse",
                    branches: {
                        $push: {
                            $cond: [
                                { $eq: ["$isArea", false] },
                                { branch: "$branch", shifts: "$shift", status: "$status" },
                                null
                            ]
                        }
                    },
                    areas: {
                        $push: {
                            $cond: [
                                { $eq: ["$isArea", true] },
                                { area: "$area", shifts: "$shift", status: "$status" },
                                null
                            ]
                        }
                    }
                }
            },
            {
                $project: {
                    branches: {
                        $filter: {
                            input: "$branches",
                            as: "branch",
                            cond: { $ne: ["$$branch", null] }
                        }
                    },
                    areas: {
                        $filter: {
                            input: "$areas",
                            as: "area",
                            cond: { $ne: ["$$area", null] }
                        }
                    },
                    _id: false
                }
            }
        ]
    }).then(data => data[0])
}