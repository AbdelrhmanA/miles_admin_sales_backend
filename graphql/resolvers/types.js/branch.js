const { mappersModelFind } = require('../../../models')
const { Types: { ObjectId } } = require('mongoose')
module.exports = {
    shifts: ({ _id: branch }, { warehouse }) => mappersModelFind({
        aggregation: [
            { $match: { branch, warehouse: new ObjectId(warehouse) } },
            { $group: { _id: "$warehouse", shifts: { $push: "$shift" }, branch: { $first: '$branch' } } },
            { $lookup: { from: 'shifts', localField: 'shifts', foreignField: '_id', as: 'shifts' } },
            { $unwind: { path: "$shifts" } },
            { $addFields: { "shifts.branch": "$branch" } },
            { $replaceRoot: { newRoot: "$shifts" } }
        ]
    })
}