const { mappersModelFind } = require('../../../models')
module.exports = {
    areas: ({ _id: warehouse }) => mappersModelFind({
        aggregation: [
            { $match: { warehouse } },
            { $group: { _id: null, area: { $addToSet: { $arrayElemAt: ["$area", -1] } } } },
            { $lookup: { from: 'areas', localField: 'area', foreignField: '_id', as: 'area' } },
            { $unwind: { path: '$area' } },
            { $replaceRoot: { newRoot: "$area" } }
        ]
    }),
    branches: ({ _id: warehouse }) => mappersModelFind({
        aggregation: [
            { $match: { warehouse } },
            { $group: { _id: null, branch: { $addToSet: "$branch" } } },
            { $lookup: { from: 'branches', localField: 'branch', foreignField: '_id', as: 'branch' } },
            { $unwind: { path: '$branch' } },
            { $replaceRoot: { newRoot: "$branch" } }
        ]
    })
}   