const { countiresModelInsert, countiresModelUpdate } = require('../../../models')

exports.addCountry = (_, { country }) => countiresModelInsert({ value: country }).then(() => true)
exports.editCountry = (_, { id, country }) =>
    countiresModelUpdate({
        one: true,
        finder: { _id: id },
        updates: { $set: country }
    }).then(({ nModified }) => nModified > 0)