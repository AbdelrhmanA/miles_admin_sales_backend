const { branchesModelAdd, branchesModelUpdate, productModelUpdateProduct, mappersModelUpdate } = require('../../../models')

exports.addBranch = (_, { branch }) => branchesModelAdd({ value: branch })
exports.editBranch = (_, { branch }) => branchesModelUpdate({ one: true, finder: { _id: branch._id }, updates: { $set: branch } })
    .then(({ nModified }) => nModified > 0)
exports.editBranchProductPrice = async (_, { branch, price, product }) => {
    let { n, nModified } = await productModelUpdateProduct({ one: true, finder: { _id: product, "price.branches.branch": branch }, update: { $set: { "price.branches.$.price": price } } })
    if (n === 0)
        return productModelUpdateProduct({ one: true, finder: { _id: product }, updates: { $push: { "price.branches": { branch, price } } } }).then(({ nModified }) => nModified > 0)
    else
        return nModified > 0
}
exports.deleteBranchProduct = async (_, { branch, product }) => {
    let { nModified } = await productModelUpdateProduct({ one: true, finder: { _id: product }, updates: { $pull: { "price.branches": { "branches.branch": branch } } } })
    let deleteFromWarehouses = await mappersModelUpdate({
        one: false, finder: { branch }, updates: { $pull: { products: { product } } }
    }).then(({ nModified, n }) => n > 0 && nModified > 0)
    return deleteFromWarehouses && nModified > 0 ? true : deleteFromWarehouses
} 