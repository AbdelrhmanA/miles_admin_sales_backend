const { genSaltSync, hashSync } = require('bcrypt')
const User = require('../../../models/Users/User')

exports.register = async (_, { user }) => {
    try {
        user.password = hashSync(user.password, genSaltSync(10))
        await new User(user).save()
        return true
    } catch (_) {
        return false
    }
    // let count = await roleModelCountRoles({ 'name': user.role })
    // if (count > 0) {
    //     user.password = hashSync("123", genSaltSync(10))
    //     return userModelInsert({ value: user })
    // } else return null
}
// exports.register = (_, { user }) => {
//     user.password = hashSync(user.password, genSaltSync(10))
//     return userModelInsert({ value: user })
// }
// exports.updateUser = (_, { user, id: _id }) =>
//     userModelUpdateOperations({ one: true, finder: { _id }, updates: { $set: user } }).then(({ nModified }) => nModified === 1)

// exports.blockUnBLock = (_, { phone, value }, { req: { user: { phone: { number } } } }) => {
//     if (number === phone) throw new Error(STATUS_BAD_REQUEST)
//     return userModelUpdateOperations({ one: true, finder: { "phone.number": phone }, updates: { $set: { isBlocked: value } } }).then(({ nModified }) => nModified === 1)
// }
// exports.addUserAddress = async (_, { phone, address }) => {
//     if (address.isDefault === true)
//         await userModelUpdateOperations({ one: true, finder: { "phone.number": phone }, updates: { $set: { "addresses.$[].isDefault": false } } })
//     return userModelUpdateOperations({ one: true, finder: { "phone.number": phone }, updates: { $push: { addresses: address } } })
//         .then(({ nModified }) => nModified > 0)
// }
// exports.deleteUserAddress = (_, { phone, id }) =>
//     userModelUpdateOperations({ one: true, finder: { "phone.number": phone }, updates: { $pull: { addresses: { _id: id } } } })
//         .then(({ nModified }) => nModified > 0)
// exports.updateUserAddress = async (_, { phone, id, address }) => {
//     if (address.isDefault === true)
//         await userModelUpdateOperations({ one: true, finder: { "phone.number": phone }, updates: { $set: { "addresses.$[].isDefault": false } } })
//     return userModelUpdateOperations({ one: true, finder: { "phone.number": phone, "addresses._id": id }, updates: { $set: { "addresses.$": address } } })
//         .then(({ nModified }) => nModified > 0)
// }
// exports.makeAddressDefault = async (_, { phone, id }) =>
//     userModelUpdateOperations({ one: false, finder: { "phone.number": phone }, updates: { $set: { "addresses.$[].isDefault": false } } })
//         .then(() =>
//             userModelUpdateOperations({ one: true, finder: { "phone.number": phone, "addresses._id": id }, updates: { $set: { "addresses.$.isDefault": true } } })
//                 .then(({ nModified }) => nModified > 0))

// exports.addUserToBranch = (_, { users, branchId, companyId }) =>
//     userModelUpdateOperations({ one: false, finder: { _id: users }, updates: { $addToSet: { comapnies: { company: companyId, branch: branchId, role: "user" } } } })
//         .then(({ nModified }) => nModified > 0)

// exports.editBranchUsers = (_, { users, branchId }) =>
//     userModelUpdateOperations({ one: false, finder: { _id: users }, updates: { $pull: { comapnies: { branch: branchId } } } })
//         .then(({ nModified }) => nModified > 0)