const { savingPath } = require('../../../utils')
module.exports = {
    single_upload: async (_, { file }) => {
        const { filename, createReadStream } = await file
        return savingPath({ filename, stream: createReadStream() })
    }
}