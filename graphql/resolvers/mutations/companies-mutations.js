const { companiesModelAdd, companiesModelUpdate } = require('../../../models/companies-model')

exports.addCompany = (_, { company }) => companiesModelAdd({ value: company })
exports.editCompany = (_, { company }) => companiesModelUpdate({ one: true, finder: { _id: company.id }, updates: { $set: company } })
    .then(({ nModified }) => nModified > 0)