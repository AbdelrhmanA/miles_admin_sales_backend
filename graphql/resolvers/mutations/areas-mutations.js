const { areasModelInsert, areasModelUpdate } = require('../../../models')

exports.addArea = (_, { area }) => {
  let pathLength = area.path ? area.path.length : 0
  area.parent = pathLength > 2 ? area.path[pathLength - 1] : undefined
  return areasModelInsert({ value: area }).then(_ => true)
}

exports.editArea = async (_, { enName, arName, id }) =>
  areasModelUpdate({ one: true, finder: { _id: id }, updates: { $set: { name: { en: enName, ar: arName } } } }).then(({ nModified }) => nModified > 0)
