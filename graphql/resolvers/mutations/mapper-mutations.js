const { mappersModelAdd, mapperModelDeleteMany, mappersModelUpdate, mappersModelFind } = require('../../../models')

exports.editCoverage = async (_, { coverage: { id: warehouse, branches = [], areas = [] } }) => {
    let finderArea = []
    let finderBranch = []
    let newCoverage = []
    for (let branch of branches)
        for (let shift of branch.shifts)
            await mappersModelFind({ one: true, finder: { branch: branch.branch, shift, warehouse }, selection: { _id: 1 } })
                .then(id => id ? finderBranch.push(id._id) : newCoverage.push({ ...branch, shift, warehouse }))
    for (let area of areas)
        for (let shift of area.shifts)
            await mappersModelFind({ one: true, finder: { area: area.area, shift, warehouse }, selection: { _id: 1 } })
                .then(id => id ? finderArea.push(id._id) : newCoverage.push({ ...area, shift, warehouse }))
    return mapperModelDeleteMany({ $or: [{ area: { $exists: true }, _id: { $nin: finderArea } }, { branch: { $exists: true }, _id: { $nin: finderBranch } }] })
        .then(_ => mappersModelAdd({ value: newCoverage, many: true }).then(done => done.length > 0))
}
exports.addProductToWarehouse = async (_, { warehouseProduct: { warehouse, areas = {}, products, branches = {} } }) => {
    if (branches.first)
        for (let branch of branches.first)
            for (let shift of branches.second)
                for (let product of products)
                    await mappersModelUpdate({
                        one: true, finder: {
                            warehouse, branch, shift,
                            products: { $not: { $elemMatch: { product: product.product } } }
                        },
                        updates: { $push: { products: product } }
                    })

    if (areas.first)
        for (let area of areas.first)
            for (let shift of areas.second)
                for (let product of products)
                    await mappersModelUpdate({
                        one: true, finder: {
                            warehouse, area, shift,
                            products: { $not: { $elemMatch: { product: product.product } } }
                        },
                        updates: { $push: { products: product } }
                    })
    return areas.first || branches.first ? true : false
}

exports.deleteWarehouseAreaProduct = (_, { _id }) =>
    mappersModelUpdate({
        one: true, finder: { "products._id": _id },
        updates: { $pull: { products: { _id: _id } } }
    }).then(({ nModified }) => nModified === 1)

exports.updateWarehouseBranchProduct = (_, { product }) =>
    mappersModelUpdate({
        one: true,
        finder: { "products._id": product._id },
        updates: {
            $set: { "products.$": product }
        }
    }).then(({ nModified }) => nModified === 1)