const { taxesModelAddTax, taxesModelUpdate, taxesModelDeleteTax } = require('../../../models')

exports.createTaxes = (_, { taxes }) => taxesModelAddTax({ many: true, value: taxes }).then(() => true)

exports.updateTax = (_, { tax }) => taxesModelUpdate({ one: true, finder: { _id: tax.id }, updates: { $set: tax } }).then(({ nModified }) => nModified > 0)

exports.deleteTax = (_, { id }) => taxesModelDeleteTax({ one: true, finder: { _id: id } }).then(({ nModified }) => nModified > 0)