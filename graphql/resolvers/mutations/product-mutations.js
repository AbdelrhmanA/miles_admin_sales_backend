const { productModelAddProduct, userModelSelectUserData, productModelUpdateProduct } = require('../../../models')

exports.addProduct = async (_, { product }, { req: { user: { _id } } }) => {
    let { name } = await userModelSelectUserData({ one: true, finder: { _id }, select: { 'name': 1 } })
    product.creation = {
        byWho: name.first + " " + name.last,
        date: new Date()
    }
    return productModelAddProduct({ value: product })
}

exports.updateProduct = (_, { product }) => productModelUpdateProduct({ one: true, finder: { _id: product.id }, updates: { $set: product } }).then(({ nModified }) => nModified > 0)