const { shiftModelUpdateShift, shiftModelAddShift } = require('../../../models')

exports.addShift = (_, { shift }) => shiftModelAddShift({ value: shift })

exports.updateShift = (_, { id: _id, shift }) => shiftModelUpdateShift({ one: true, finder: { _id }, updates: { $set: shift } })
    .then(({ nModified }) => nModified > 0)

exports.toggleShiftState = (_, { id: _id, value }) => shiftModelUpdateShift({ one: true, finder: { _id }, updates: { $set: { isDisabled: value } } })
    .then(({ nModified }) => nModified > 0)

exports.addTimeSlot = (_, { id: _id, timeSlot }) => shiftModelUpdateShift({ one: true, finder: { _id }, updates: { $push: { timeSlots: timeSlot } } })
    .then(({ nModified }) => nModified > 0)

exports.deleteTimeSlot = (_, { shiftId: _id, slotId }) => shiftModelUpdateShift({ one: true, finder: { _id }, updates: { $pull: { timeSlots: { _id: slotId } } } })
    .then(({ nModified }) => nModified > 0)

exports.updateTimeSlot = (_, { shiftId: _id, slotId, timeSlot }) => shiftModelUpdateShift({ one: true, finder: { _id, "timeSlots._id": slotId }, updates: { $set: { "timeSlots.$": timeSlot } } })
    .then(({ nModified }) => nModified > 0)
