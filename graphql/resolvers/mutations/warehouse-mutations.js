const { warehousesModelAdd, warehousesModelUpdate } = require('../../../models')

exports.addWarehouse = (_, { warehouse }) => warehousesModelAdd({ value: warehouse })
exports.updateWarehouse = (_, { warehouse, id: _id }) =>
    warehousesModelUpdate({ one: true, finder: { _id }, updates: { $set: warehouse } }).then(({ nModified }) => nModified === 1)