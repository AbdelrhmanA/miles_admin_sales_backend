const { ingredientModelCreateIngredient, ingredientModelUpdateIngredient, ingredientModelDeleteIngredient } = require('../../../models')

exports.createIngredient = (_, { ingredient }) => ingredientModelCreateIngredient({ value: ingredient })

exports.updateIngredient = (_, { ingredient, id }) => ingredientModelUpdateIngredient({ finder: { _id: id }, updates: { $set: { name: ingredient.name } } }).then(({ nModified }) => nModified === 1)

exports.deleteIngrdient = (_, { id }) => ingredientModelDeleteIngredient(id).then(({ deletedCount }) => deletedCount == 1)

