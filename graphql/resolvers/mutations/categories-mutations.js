const { categoriesModelInsert, categoriesModelUpdate, productModelSelectProducts, categoriesModelDelete } = require('../../../models')
const { productModelUpdateProduct } = require('../../../models/product-model')

exports.addCategory = async (_, { category }) => {
    let message = undefined
    let { path } = category
    if (path && path.length > 0) {
        category.parent = path[path.length - 1]
        let products = await productModelSelectProducts({
            aggregation: [
                { $match: { $expr: { $in: [path[path.length - 1], { $slice: ["$category", -1] }] } } },
                { $group: { _id: null, products: { $push: "$_id" } } }
            ]
        }).then(([products]) => products ? products.products : undefined)
        if (products) {
            let { _id } = await categoriesModelInsert({ value: { name: { en: "Other", ar: "آخر" }, parent: category.parent } })
            path.push(_id)
            await productModelUpdateProduct({ finder: { _id: products }, updates: { $set: { category: path } } })
            message = "All other products moved to new category (Other)"
        }
    }
    else category.parent = null
    return categoriesModelInsert({ value: category }).then(() => message)
}

exports.editCategory = async (_, { category }) => {
    let { path } = category
    category.parent = path && path.length > 0 ? path[path.length - 1] : null
    return categoriesModelUpdate({ one: true, finder: { _id: category.id }, updates: { $set: category } })
        .then(({ nModified }) => nModified > 0)
}

exports.deleteCategory = (_, { id }) =>
    categoriesModelDelete({ finder: { _id: id } }).then(({ n, ok }) => n == 1 && ok == 1)