module.exports = {
    Query: require('./queries'),
    Mutation: require('./mutations'),
    // ...require('./types.js')
}