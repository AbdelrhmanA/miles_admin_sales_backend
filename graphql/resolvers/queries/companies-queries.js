const { companiesModelFind } = require('../../../models')

exports.getCompaniesList = () => companiesModelFind({
    aggregation: [
        {
            $lookup: {
                from: "branches",
                let: { id: "$_id" },
                pipeline: [
                    { $match: { $expr: { $eq: ["$$id", "$company"] } } },
                    { $project: { _id: 1 } }
                ],
                as: 'branchesCount'
            }
        },
        {
            $addFields: { branchesCount: { $size: '$branchesCount' } }
        }
    ]
})
exports.getCompanyDetails = (_, { id }) => companiesModelFind({ one: true, finder: { _id: id } })