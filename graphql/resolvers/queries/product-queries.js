const { productModelSelectProducts } = require('../../../models')

exports.getAllProducts = () =>
    productModelSelectProducts({
        aggregation: [
            {
                $lookup: {
                    from: 'categories',
                    let: { c: '$category' },
                    pipeline: [
                        {
                            $match: { $expr: { $in: ["$_id", '$$c'] } }
                        },
                        {
                            $project: {
                                _id: 0,
                                name: "$name.en"
                            }
                        }],
                    as: 'category'
                }
            },
            {
                $unwind: {
                    path: '$category',
                    preserveNullAndEmptyArrays: true
                }
            },
            {
                $group: {
                    _id: "$_id",
                    category: { $push: "$category.name" },
                    name: { $first: "$name" },
                    status: { $first: "$status" },
                    price: { $first: "$price" },
                    private: { $first: "$private" },
                }
            }
        ]
    })

exports.getProductDetails = (_, { id }) => productModelSelectProducts({ one: true, finder: { _id: id } })

exports.productsNames = () => productModelSelectProducts({
    aggregation: [
        {
            $project: {
                value: "$_id",
                label: "$name.en"
            }
        }
    ]
})
