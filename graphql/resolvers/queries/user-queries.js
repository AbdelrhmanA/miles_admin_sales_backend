const passport = require('passport')
const { userModelSelectUserData } = require('../../../models')

exports.login = (_, args, { req }) => new Promise((resolve) => {
    req.body = args
    passport.authenticate('local', (_, user, info) => !user ? resolve(info) : req.login(user, () => resolve(user)))(req)
})
exports.logout = (_, __, { req, res }) => {
    if (!req.user) return false
    res.clearCookie('connect.sid')
    req.session.destroy()
    req.logout()
    return true
}
exports.getAllUsers = () => userModelSelectUserData({})
exports.getUserDetails = (_, { phone }, __) => userModelSelectUserData({ one: true, finder: { "phone.number": phone } })
exports.getUserAddresses = (_, { phone }) =>
    userModelSelectUserData({ one: true, finder: { "phone.number": phone }, select: { _id: 0, addresses: 1 } }).then(({ addresses }) => addresses)