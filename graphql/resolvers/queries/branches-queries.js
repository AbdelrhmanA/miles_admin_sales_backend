const { Types: { ObjectId } } = require('mongoose')
const { branchesModelFind, mappersModelFind } = require('../../../models')

exports.getBranches = (_, { company }) => branchesModelFind({ finder: { company } })
exports.getBranchDetails = (_, { branch }) =>
  branchesModelFind({
    aggregation: [
      {
        '$match': {
          '_id': new ObjectId(branch)
        }
      }, {
        '$lookup': {
          'from': 'companies',
          'let': {
            'company': '$company'
          },
          'pipeline': [
            {
              '$match': {
                '$expr': {
                  '$eq': [
                    '$$company', '$_id'
                  ]
                }
              }
            }, {
              '$project': {
                'name': 1,
                '_id': 1,
                'logo': 1
              }
            }
          ],
          'as': 'company'
        }
      }, {
        '$unwind': {
          'path': '$company'
        }
      }, {
        '$addFields': {
          'id': {
            '$toString': '$_id'
          }
        }
      }, {
        '$lookup': {
          'from': 'users',
          'let': {
            'branchId': '$_id'
          },
          'pipeline': [
            {
              '$match': {
                '$expr': {
                  '$in': [
                    '$$branchId', '$comapnies.branch'
                  ]
                }
              }
            }, {
              '$project': {
                'name': 1
              }
            }
          ],
          'as': 'users'
        }
      }, {
        '$lookup': {
          'from': 'mappers',
          'let': {
            'id': '$_id'
          },
          'pipeline': [
            {
              '$match': {
                '$expr': {
                  '$eq': [
                    '$$id', '$branch'
                  ]
                }
              }
            }, {
              '$project': {
                'warehouse': 1,
                '_id': 0,
                'shift': 1
              }
            }
          ],
          'as': 'warehouses'
        }
      }, {
        '$lookup': {
          'from': 'shifts',
          'localField': 'warehouses.shift',
          'foreignField': '_id',
          'as': 'attachedShifts'
        }
      }, {
        '$lookup': {
          'from': 'warehouses',
          'localField': 'warehouses.warehouse',
          'foreignField': '_id',
          'as': 'warehouses'
        }
      }, {
        '$addFields': {
          'warehouses': '$warehouses.name',
          'attachedShifts': '$attachedShifts.name'
        }
      }
    ]
  }).then(data => data ? data[0] : null)
exports.getBranchesTree = (_, { warehouse }) => warehouse ?
  mappersModelFind({
    aggregation: [
      { $match: { warehouse: ObjectId(warehouse) } },
      {
        $lookup: {
          from: 'branches',
          localField: 'branch',
          foreignField: '_id',
          as: 'branch'
        }
      },
      { $unwind: { path: "$branch" } },
      {
        $group: {
          _id: "$branch._id",
          label: { $first: "$branch.name.en" },
          value: { $first: "$branch._id" }
        }
      }
    ]
  }) :
  branchesModelFind({
    aggregation: [
      {
        $project: {
          _id: 0,
          value: "$_id",
          label: "$name.en",
        }
      }
    ]
  })
exports.getBranchProducts = (_, { branch }) => mappersModelFind({
  aggregation: [
    {
      '$match': {
        'branch': new ObjectId(branch)
      }
    }, {
      '$unwind': {
        'path': '$products'
      }
    }, {
      '$lookup': {
        'from': 'products',
        'localField': 'products.product',
        'foreignField': '_id',
        'as': 'productsDetails'
      }
    }, {
      '$project': {
        'products': 1,
        'productsDetails': {
          '$arrayElemAt': [
            '$productsDetails', 0
          ]
        },
        'branchId': '$branch'
      }
    }, {
      '$group': {
        '_id': '$products.product',
        'stockbased': {
          '$first': '$products.stockBased'
        },
        'visiablity': {
          '$first': '$products.visiablity'
        },
        'stock': {
          '$first': '$products.stock'
        },
        'price': {
          '$first': {
            '$arrayElemAt': [
              {
                '$filter': {
                  'input': '$productsDetails.price.branches',
                  'as': 'price',
                  'cond': {
                    '$eq': [
                      '$$price.branch', '$branchId'
                    ]
                  }
                }
              }, 0
            ]
          }
        },
        'name': {
          '$first': '$productsDetails.name'
        },
        'branchId': {
          '$first': '$branchId'
        },
        'productId': {
          '$first': '$productsDetails._id'
        }
      }
    }, {
      '$project': {
        '_id': 0,
        'stockbased': 1,
        'visiablity': 1,
        'stock': 1,
        'price': '$price.price',
        'name': 1,
        'branchId': 1,
        'productId': 1
      }
    }
  ]
})
