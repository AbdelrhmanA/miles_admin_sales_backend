const { categoriesModelFind } = require('../../../models')
const { Types: { ObjectId } } = require('mongoose')

exports.getSubCategories = (_, { parent }) =>
    categoriesModelFind({
        aggregation: [
            { $match: { parent: parent ? new ObjectId(parent) : undefined } },
            {
                $lookup: {
                    from: 'products',
                    let: { id: "$_id" },
                    pipeline: [
                        { $match: { $expr: { $in: ["$$id", { $slice: ["$category", -1] }] } } },
                        { $project: { _id: 1 } }
                    ],
                    as: 'childrenProducts'
                }
            },
            {
                $graphLookup: {
                    from: 'categories',
                    startWith: "$_id",
                    connectFromField: '_id',
                    connectToField: 'parent',
                    as: 'subsCount',
                }
            },
            {
                $addFields: {
                    subsCount: { $size: '$subsCount' },
                    childrenProducts: { $size: "$childrenProducts" }
                }
            },
            {
                $graphLookup: {
                    from: 'categories',
                    startWith: "$parent",
                    connectFromField: 'parent',
                    connectToField: '_id',
                    as: 'parents'
                }
            },
            {
                $unwind: {
                    path: "$parents",
                    preserveNullAndEmptyArrays: true
                }
            },
            {
                $group: {
                    _id: "$_id",
                    parents: { $push: "$parents._id" },
                    name: { $first: "$name" },
                    parent: { $first: "$parent" },
                    customDisplay: { $first: "$customDisplay" },
                    subsCount: { $first: "$subsCount" },
                    childrenProducts: { $first: "$childrenProducts" },
                    webImage: { $first: "$webImage" },
                    mobileImage: { $first: "$mobileImage" }
                }
            },
            {
                $sort: {
                    "name.en": 1
                }
            }
        ]
    })

exports.listCategoriesNames = () => categoriesModelFind({
    aggregation: [
        {
            $match: { parent: null }
        },
        {
            $project: {
                value: "$_id",
                label: "$name.en",
            }
        }
    ]
})

exports.getCategoryDetails = (_, { _id }) =>
    categoriesModelFind({
        aggregation: [
            {
                $match: { _id: new ObjectId(_id) }
            },
            {
                $graphLookup: {
                    from: 'categories',
                    startWith: "$parent",
                    connectFromField: 'parent',
                    connectToField: '_id',
                    as: 'path'
                }
            },
            {
                $unwind: {
                    path: '$path',
                    preserveNullAndEmptyArrays: true
                }
            },
            {
                $group: {
                    _id: "$_id",
                    path: {
                        $push: "$path._id"
                    },
                    name: {
                        $first: "$name"
                    },
                    description: {
                        $first: "$description"
                    },
                    webImage: {
                        $first: "$webImage"
                    },
                    mobileImage: {
                        $first: "$mobileImage"
                    },
                    parent: {
                        $first: "$parent"
                    },
                    customDisplay: {
                        $first: "$customDisplay"
                    }
                }
            }
        ]
    }).then(categories => categories.pop())