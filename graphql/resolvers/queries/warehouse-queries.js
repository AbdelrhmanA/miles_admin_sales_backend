const { warehousesModelFind } = require('../../../models')

exports.listWarehouses = () => warehousesModelFind({
    aggregation: [
        {
            $lookup: {
                from: 'mappers',
                localField: '_id',
                foreignField: 'warehouse',
                as: 'products'
            }
        },
        {
            $addFields: {
                products: "$products.products.product"
            }
        },
        {
            $unwind: {
                path: "$products",
                preserveNullAndEmptyArrays: true
            }
        },
        {
            $unwind: {
                path: "$products",
                preserveNullAndEmptyArrays: true
            }
        },
        {
            $group: {
                _id: "$_id",
                sum: {
                    $addToSet: "$products"
                },
                name: { $first: "$name" },
                status: { $first: "$status" }
            }
        },
        {
            $addFields: {
                sum: { $size: "$sum" }
            }
        }
    ]
})

exports.warehouseDetails = (_, { _id }) => warehousesModelFind({ one: true, finder: { _id } })

exports.getCoverageInitials = () => ({})

exports.getAddProductToWarehouseInitials = () => ({})