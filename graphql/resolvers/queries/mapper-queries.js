const { Types: { ObjectId } } = require('mongoose')
const { mappersModelFind } = require('../../../models')



exports.getWarehouseAreasProducts = (_, { warehouse, shift, area }) =>
  mappersModelFind({
    aggregation: [
      {
        '$project': {
          'area': {
            '$arrayElemAt': [
              '$area', -1
            ]
          },
          'warehouse': 1,
          'shift': 1,
          'products': 1,
          'status': 1
        }
      }, {
        '$match': {
          'warehouse': new ObjectId(warehouse),
          'shift': new ObjectId(shift),
          'area': area
        }
      }, {
        '$unwind': {
          'path': '$products'
        }
      }, {
        '$addFields': {
          'productId': '$products.id'
        }
      }, {
        '$lookup': {
          'from': 'products',
          'localField': 'productId',
          'foreignField': '_id',
          'as': 'productDetails'
        }
      }, {
        '$project': {
          'name': {
            '$arrayElemAt': [
              '$productDetails.name.en', -1
            ]
          },
          'warehouse': 1,
          'shift': 1,
          'area': 1,
          'products': 1,
          '_id': 0
        }
      }, {
        '$group': {
          '_id': {
            'warehouse': '$warehouse',
            'area': '$area',
            'shift': '$shift'
          },
          'products': {
            '$push': {
              'details': '$products',
              'name': '$name',
              'area': '$area',
              'shift': '$shift',
              "warehouse": "$warehouse"
            }
          }
        }
      }
    ]
  })
