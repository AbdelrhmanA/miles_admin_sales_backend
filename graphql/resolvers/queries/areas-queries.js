const { areasModelFind, countiresModelFind } = require('../../../models')
const { Types: { ObjectId } } = require('mongoose')

exports.getCityAreas = (_, { city }) =>
    areasModelFind({
        aggregation:
            [
                { $match: city ? { "path.1": city, parent: { $exists: false } } : { parent: { $exists: false } } },
                {
                    $graphLookup: {
                        from: 'areas',
                        startWith: "$_id",
                        connectFromField: '_id',
                        connectToField: 'parent',
                        as: 'subsCount'
                    }
                },
                { $addFields: { subsCount: { $size: '$subsCount' } } },
                { $project: { path: 0 } }
            ]
    })

exports.getSubAreas = (_, { area }) => areasModelFind({
    aggregation: [
        { $match: { parent: new ObjectId(area) } },
        {
            $graphLookup: {
                from: 'areas',
                startWith: "$_id",
                connectFromField: '_id',
                connectToField: 'parent',
                as: 'subsCount'
            }
        },
        { $addFields: { subsCount: { $size: '$subsCount' } } },
        { $project: { path: 0 } }
    ]
})

exports.getAreaTree = () => countiresModelFind({
    aggregation: [
        {
            $project: {
                _id: 0,
                value: "$_id",
                label: "$name.en",
                children: {
                    $map: {
                        input: "$cities",
                        as: "sub",
                        in: {
                            value: "$$sub._id",
                            label: "$$sub.name.en",
                            city: true
                        }
                    }
                }
            }
        }
    ]
})