const { shiftModelfindShifts, mappersModelFind } = require('../../../models')
const { Types: { ObjectId } } = require('mongoose')

exports.getAllShifts = () => shiftModelfindShifts({})

exports.getShiftsTree = () => shiftModelfindShifts({
    aggregation: [
        {
            $project: {
                _id: 0,
                value: "$_id",
                label: "$name",
            }
        }
    ]
})
