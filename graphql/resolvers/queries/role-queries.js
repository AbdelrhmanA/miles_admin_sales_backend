const { roleModelSelectRoles } = require('../../../models')
const { permissions } = require('../../../utils')

exports.getRoles = () => roleModelSelectRoles({ one: false})

exports.getPermissions = () => Object.values(permissions)