const { countiresModelFind } = require('../../../models')

exports.getAllCountries = () => countiresModelFind({
    aggregation: [
        {
            $lookup: {
                from: "areas",
                let: { "id": "$_id" },
                pipeline: [
                    { $addFields: { country: { $arrayElemAt: ["$path", 0] } } },
                    { $match: { $expr: { $eq: ["$country", "$$id"] } } },
                    {
                        $project: {
                            _id: 0,
                            path: { $arrayElemAt: ["$path", 1] }
                        }
                    }
                ],
                as: "areas"
            }
        },
        { $unwind: { path: "$cities", preserveNullAndEmptyArrays: true } },
        {
            $addFields: {
                'cities.countAreas': {
                    $size: {
                        $filter: {
                            input: "$areas",
                            as: "area",
                            cond: { $eq: ["$$area.path", "$cities._id"] }
                        }
                    }
                },
                countAreas: { $size: "$areas" }
            }
        },
        {
            $group: {
                _id: "$_id",
                cities: { $push: "$cities" },
                name: { $first: '$name' },
                code: { $first: '$code' },
                countAreas: { $first: '$countAreas' },
            }
        },
        { $addFields: { countCities: { $size: "$cities" } } }
    ]
})
exports.getCountryShortNames = () => countiresModelFind({
    aggregation: [
        {
            $project: { _id: 0, label: "$name.short", value: "$_id" }
        }
    ]
})