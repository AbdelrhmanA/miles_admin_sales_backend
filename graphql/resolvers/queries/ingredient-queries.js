const { ingredientModelSelectIngredient } = require('../../../models')
exports.getAllIngredients = () => ingredientModelSelectIngredient({
    aggregation: [
        {
            $lookup: {
                from: 'products',
                let: { id: "$_id" },
                pipeline: [
                    { $match: { $expr: { $eq: ["$$id", "$productIngredients.name"] } } },
                    { $project: { _id: 1 } }
                ],
                as: 'products'
            }
        },
        {
            $addFields: { deletable: { $eq: [{ $size: "$products" }, 0] } }
        }
    ]
})
