const { taxesModelFind, countiresModelFind } = require('../../../models')

exports.getTaxesList = () => taxesModelFind({
    aggregation: [
        {
            $lookup: {
                from: 'products',
                let: { id: "$_id" },
                pipeline: [
                    { $match: { $expr: { $eq: ["$taxes.tax", "$$id"] } } },
                    { $project: { _id: 1 } }
                ],
                as: 'deletable'
            }
        },
        {
            $lookup: {
                from: 'countries',
                let: { country: "$countryCode" },
                pipeline: [
                    { $match: { $expr: { $eq: ["$_id", "$$country"] } } },
                    { $project: { name: "$name.short", _id: 0 } }
                ],
                as: 'countryCode'
            }
        },
        {
            $addFields: { deletable: { $lt: [{ $size: "$deletable" }, 1] } }
        },
        {
            $group: {
                _id: { $arrayElemAt: ["$countryCode.name", 0] },
                count: { $sum: 1 },
                taxes: { $push: "$$ROOT" },
            }
        }
    ],
})

exports.getTaxesTree = () => countiresModelFind({
    aggregation: [
        {
            $project: {
                _id: 0,
                "label": "$name.short",
                "value": "$_id"
            }
        },
        {
            $lookup: {
                from: 'taxes',
                let: { code: "$value" },
                pipeline: [
                    { $match: { $expr: { $eq: ['$$code', "$countryCode"] } } },
                    {
                        $project: {
                            _id: 0,
                            value: "$_id",
                            label: "$name.en"
                        }
                    }
                ],
                as: 'children'
            }
        },
        {
            $match: { $expr: { $gt: [{ $size: "$children" }, 0] } }
        }
    ]
})